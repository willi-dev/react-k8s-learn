#!/usr/bin/env bash

K8S_PROJECT_ID=${1}
SERVICE_NAME=${2}
BUILD_NUMBER=${3}
NAMESPACE=${4}

sed -e "s/K8S_PROJECT_ID/$K8S_PROJECT_ID/g" \
  -e "s/SERVICE_NAME/$SERVICE_NAME/g" \
  -e "s/BUILD_NUMBER/$BUILD_NUMBER/g" \
  -e "s/NAMESPACE/$NAMESPACE/g" \
  ./etc/k8s/deployment.yaml > ./etc/k8s/temp.deployment.yaml

kubectl apply -f ./etc/k8s/temp.deployment.yaml