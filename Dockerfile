FROM node:12-alpine as build

WORKDIR /app

# Build Packages
COPY package.json ./
COPY yarn.lock ./
RUN yarn --production

COPY . ./
RUN yarn build

# production environment
FROM nginx:stable-alpine
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]